

#########################################################

# Functions kindly supplied by Martin P. Boer

#########################################################

  # CREATE FUNCTION TO FIT LOGISTIC AND SPLINE CURVES (by Martin Boer)
          require(splines)
          PsplinesREML <- function(x,y,xmin,xmax, nknots=100,lambda=1.0,optimize=TRUE)
          {
            eps = 0.001
            xmin = xmin - eps
            xmax = xmax + eps
            pord = 2
            degree = 3
            nseg = nknots-3
            dx = (xmax - xmin) / nseg
            knots = seq(xmin - degree * dx, xmax + degree * dx, by = dx)
            B = splineDesign(knots, x, derivs=rep(0,length(x)), ord = degree + 1)
            BtB = crossprod(B)
            # max ED for random part.. 
            max_ED = length(unique(x)) - pord
            BtY = t(B) %*% y
            ncolB = ncol(B)
            D = diff(diag(1,ncolB),diff=2)
            DtD = crossprod(D)
            phi = 1.0
            psi = lambda*phi
            
            n = length(x)
            p = 2
            
            for (it in 1:100)
            {
              C = phi*BtB + psi*DtD
              
              # calculate EDs 
              Cinv = solve(C)
              EDf_spline = p
              EDr_spline = ncolB-p - psi*sum(diag(Cinv%*%DtD))
              EDres = n - p - EDr_spline
              #cat(sprintf("%4d %6.4f %5.4f \n", it, EDr_spline, EDres))
              a = phi*Cinv%*%BtY
              r = y - B %*% a
              
              if (optimize==FALSE) break
              
              Da = D %*% a
              psi_new = EDr_spline / (sum(Da^2) + 1.0e-6)
              phi_new = EDres / sum(r^2)
              
              pold = log(c(phi,psi))
              pnew = log(c(phi_new,psi_new))
              dif = max(abs(pold-pnew))
              phi = phi_new
              psi = psi_new

              if (dif < 1.0e-5) break
            }
            L = list(max_ED=max_ED, ED=EDr_spline+p,a=a,knots=knots,Nobs=n,x=x,y=y,xmin=xmin,xmax=xmax,
                     optimize=optimize)
            class(L) = "PsplinesREML"
            L
          }
          
          predict.PsplinesREML <- function(obj, x, deriv = FALSE)
          {
            d= ifelse(deriv,1,0)
            Bgrid = splineDesign(obj$knots, x, derivs=rep(d,length(x)), ord = 4)
            pred = as.vector(Bgrid %*% obj$a)
            pred
          }
          
          
          summary.PsplinesREML <- function(obj)
          {
            if(obj$optimize)
            {
              cat("P-splines mixed model analysis \n\n")  
            } else {
              cat("P-splines analysis, with fixed penalty \n\n")
            }
            cat("Dimensions: \n")
            col.names <- c("Effective", "Maximum", "Ratio", "Type")
            row.names <- c("Intercept", "slope", "f(x)", NA, "Total", "Residual", "Nobs")	
            m <- matrix(ncol = 4, nrow = 3 + 4, dimnames = list(row.names,col.names))
            ed = c(1,1,obj$ED-2)
            res = c(1.0,2.0,obj$Nobs)
            m[,1] = c(sprintf("%.1f",ed),NA,sprintf("%.1f", obj$ED),
                      sprintf("%.1f", obj$Nobs - obj$ED), 
                      sprintf("%.0f", obj$Nobs))
            m[,2] = c(sprintf("%.0f",c(1,1,obj$max_ED)),NA,sprintf("%0.f",obj$max_ED+2),NA,NA)
            m[,3] = c(sprintf("%.2f",c(1,1,(obj$ED-2)/obj$max_ED)),NA,
                      sprintf("%.2f",obj$ED/(obj$max_ED+2)), NA,NA)
            m[,4] = c('F','F','S',rep(NA,4))
            print(m, quote = FALSE, right = TRUE, na.print = "", print.gap = 5)	
          }
          
		  
		  
		    
          # logistic function
          logistic <- function(t, theta) {
            theta[1]/(1+exp(-theta[2]*(t-theta[3])))
          }
          
          # derivative logistic function.
          logistic_deriv <- function(t, theta)
          {
            theta[1]*theta[2]*exp(-theta[2]*(t-theta[3]))/(1+exp(-theta[2]*(t-theta[3])))^2
          }
          
          # Second derivative logistic function
          logistic_deriv2 <- function(t, theta)
          {
            theta[1]*(theta[2]^2)*exp(-2*theta[2]*(t-theta[3]))*(-exp(theta[2]*(t-theta[3]))+1)/(1+exp(-theta[2]*(t-theta[3])))^3
          }