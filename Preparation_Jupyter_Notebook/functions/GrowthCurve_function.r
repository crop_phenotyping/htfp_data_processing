library(doParallel)

#====================================================================================#
#         Fit growth curves based on spatially corrected phenotype data              #
#====================================================================================#

growthCurve <- function(data,          # data set
                        trial,         # name of column denoting trial
                        genotype,      # genotype
                        timevar,       # time component over which curves are fitted (e.g. GDD)
                        timeInterval = 1,  # predict trait (height) at x intervals (defaults at 1 for every day,
                                           # but when GDD is used, 10 may be more apprpriate
                        trait,         # trait to be fitted over time (height)
                        rep,           # replicate ('none' if not applicable)
                        plot,          # plot number (important when genotypes have multiple plots within a replicate); 'none' if not applicable
                        parallel = TRUE)  # Whether or not run in parallel (only really needed when number of trials is large)
{
  #Temporary change of var. names
  names(data)[names(data)==trial]    <- 'trial'
  names(data)[names(data)==genotype] <- 'genotype'
  names(data)[names(data)==timevar]  <- 'timevar'
  names(data)[names(data)==trait]    <- 'trait'
  if(plot != "none"){
  names(data)[names(data)==plot]     <- 'plot'
  } 
  if(rep != "none"){
  names(data)[names(data)==rep]      <- 'rep'
  }

  # Parallel or sequential?
  if(parallel == FALSE){
    registerDoSEQ()
    `%fun%` <- `%do%`
  } else {
    cl <- makePSOCKcluster(detectCores()-1)
    registerDoParallel(cl)
    `%fun%` <- `%dopar%`
  }
  
  ## Loop over experiments
  Results <- foreach(exp = unique(data$trial),   
                     .combine=rbind,             
                     .packages=c('doParallel','plyr')) %fun% {
                                     
    
    # Combine names to create  unique identifier for each plot
    if(rep != "none" & plot != "none"){
      data$UniqueID <- paste(data$genotype,data$rep,data$plot, sep='_')
      uniqueID_constitute = 'GenoRepPlot'
    } else if(rep != "none" & plot == "none"){
      data$UniqueID <- paste(data$genotype,data$rep, sep='_')
      uniqueID_constitute = 'GenoRep'
    } else if(rep == "none" & plot != "none"){
      data$UniqueID <- paste(data$genotype,data$plot, sep='_') 
      uniqueID_constitute = 'GenoPlot'
    } else {
      data$UniqueID <- data$genotype
      uniqueID_constitute = 'Geno'
    }
                       
    # Subset data based on experiment:
    df <- subset(data, trial == exp & is.na(trait)==F) 
                                     
    # Loop over unique identifiers
    res1 <- foreach(unid = unique(df$UniqueID), .combine=rbind, .errorhandling = 'remove') %do% {
      
      # Subset based on unique identifier
      s <- subset(df, UniqueID == unid)
          
          # Define x and y
          x <- s$timevar; y <- s$trait
          
          #=================================================================================#
          # CREATE FUNCTION TO FIT LOGISTIC AND SPLINE CURVES (by Martin Boer)
          require(splines)
          PsplinesREML <- function(x,y,xmin,xmax, nknots=100,lambda=1.0,optimize=TRUE)
          {
            eps = 0.001
            xmin = xmin - eps
            xmax = xmax + eps
            pord = 2
            degree = 3
            nseg = nknots-3
            dx = (xmax - xmin) / nseg
            knots = seq(xmin - degree * dx, xmax + degree * dx, by = dx)
            B = splineDesign(knots, x, derivs=rep(0,length(x)), ord = degree + 1)
            BtB = crossprod(B)
            # max ED for random part.. 
            max_ED = length(unique(x)) - pord
            BtY = t(B) %*% y
            ncolB = ncol(B)
            D = diff(diag(1,ncolB),diff=2)
            DtD = crossprod(D)
            phi = 1.0
            psi = lambda*phi
            
            n = length(x)
            p = 2
            
            for (it in 1:100)
            {
              C = phi*BtB + psi*DtD
              
              # calculate EDs 
              Cinv = solve(C)
              EDf_spline = p
              EDr_spline = ncolB-p - psi*sum(diag(Cinv%*%DtD))
              EDres = n - p - EDr_spline
              #cat(sprintf("%4d %6.4f %5.4f \n", it, EDr_spline, EDres))
              a = phi*Cinv%*%BtY
              r = y - B %*% a
              
              if (optimize==FALSE) break
              
              Da = D %*% a
              psi_new = EDr_spline / (sum(Da^2) + 1.0e-6)
              phi_new = EDres / sum(r^2)
              
              pold = log(c(phi,psi))
              pnew = log(c(phi_new,psi_new))
              dif = max(abs(pold-pnew))
              phi = phi_new
              psi = psi_new

              if (dif < 1.0e-5) break
            }
            L = list(max_ED=max_ED, ED=EDr_spline+p,a=a,knots=knots,Nobs=n,x=x,y=y,xmin=xmin,xmax=xmax,
                     optimize=optimize)
            class(L) = "PsplinesREML"
            L
          }
          
          predict.PsplinesREML <- function(obj, x, deriv = FALSE)
          {
            d= ifelse(deriv,1,0)
            Bgrid = splineDesign(obj$knots, x, derivs=rep(d,length(x)), ord = 4)
            pred = as.vector(Bgrid %*% obj$a)
            pred
          }
          
          
          summary.PsplinesREML <- function(obj)
          {
            if(obj$optimize)
            {
              cat("P-splines mixed model analysis \n\n")  
            } else {
              cat("P-splines analysis, with fixed penalty \n\n")
            }
            cat("Dimensions: \n")
            col.names <- c("Effective", "Maximum", "Ratio", "Type")
            row.names <- c("Intercept", "slope", "f(x)", NA, "Total", "Residual", "Nobs")	
            m <- matrix(ncol = 4, nrow = 3 + 4, dimnames = list(row.names,col.names))
            ed = c(1,1,obj$ED-2)
            res = c(1.0,2.0,obj$Nobs)
            m[,1] = c(sprintf("%.1f",ed),NA,sprintf("%.1f", obj$ED),
                      sprintf("%.1f", obj$Nobs - obj$ED), 
                      sprintf("%.0f", obj$Nobs))
            m[,2] = c(sprintf("%.0f",c(1,1,obj$max_ED)),NA,sprintf("%0.f",obj$max_ED+2),NA,NA)
            m[,3] = c(sprintf("%.2f",c(1,1,(obj$ED-2)/obj$max_ED)),NA,
                      sprintf("%.2f",obj$ED/(obj$max_ED+2)), NA,NA)
            m[,4] = c('F','F','S',rep(NA,4))
            print(m, quote = FALSE, right = TRUE, na.print = "", print.gap = 5)	
          }
          
          # logistic function
          logistic <- function(t, theta) {
            theta[1]/(1+exp(-theta[2]*(t-theta[3])))
          }
          
          # derivative logistic function.
          logistic_deriv <- function(t, theta)
          {
            theta[1]*theta[2]*exp(-theta[2]*(t-theta[3]))/(1+exp(-theta[2]*(t-theta[3])))^2
          }
          
          # Second derivative logistic function
          logistic_deriv2 <- function(t, theta)
          {
            theta[1]*(theta[2]^2)*exp(-2*theta[2]*(t-theta[3]))*(-exp(theta[2]*(t-theta[3]))+1)/(1+exp(-theta[2]*(t-theta[3])))^3
          }
          # END PsplinesREML function
          #===================================================================================#
          
          
          
          # splines model
          obj <- PsplinesREML(x=x, y=y, xmin=min(x), xmax=max(x))
          
          # Define range of GDD values used in the fitting (round by 10 GDD)
          time = seq(round_any(min(x),timeInterval, f= ceiling),round_any(max(x),
                     timeInterval, f= floor), timeInterval) 
          prTrait_S <- predict(obj, time)
          GrowthRate_S = predict(obj, time, deriv=TRUE)   
          MaxRate_S = max(GrowthRate_S)                  
          Asymptote_S = max(prTrait_S)             
          InflPoint_S = time[which.max(GrowthRate_S)]     
                                
          # logistic model
          dfr <- data.frame(time=x, trait=y)
          # logistic model to get start values
          obj.nls <- nls(trait ~ SSlogis(time,phi1,phi2,phi3), data=dfr)
          cf = coef(obj.nls)                            
          alpha = as.numeric(cf[1])   
          beta = as.numeric(1/cf[3])  
          gamma = as.numeric(cf[2])
          theta = c(alpha, beta, gamma)
          Asymptote_L = alpha                        
          MaxRate_L = 0.25*alpha*beta                  
          InflPoint_L = gamma                           
          prTrait_L = logistic(time, theta)               
          GrowthRate_L = logistic_deriv(time, theta)
	        Acceleration_L <- logistic_deriv2(time, theta)
	        
          # Combine data in data frame
          output <- data.frame(trial = exp,
                     UniqueID = unid,
                     time, prTrait_S, GrowthRate_S, MaxRate_S, Asymptote_S, InflPoint_S, 
                     prTrait_L, GrowthRate_L, MaxRate_L, Acceleration_L, Asymptote_L, InflPoint_L)
          if(uniqueID_constitute == 'GenoRepPlot'){
            output$genotype <- sapply(strsplit(as.character(output$UniqueID),'_'), "[", 1)
            output$rep <- sapply(strsplit(as.character(output$UniqueID),'_'), "[", 2)
            output$plot <- sapply(strsplit(as.character(output$UniqueID),'_'), "[", 3)
          } else if(uniqueID_constitute == 'GenoRep'){
            output$genotype <- sapply(strsplit(as.character(output$UniqueID),'_'), "[", 1)
            output$rep <- sapply(strsplit(as.character(output$UniqueID),'_'), "[", 2)  
          } else if(uniqueID_constitute == 'GenoPlot'){
            output$genotype <- sapply(strsplit(as.character(output$UniqueID),'_'), "[", 1)
            output$plot <- sapply(strsplit(as.character(output$UniqueID),'_'), "[", 2)  
          } else {
            output$genotype <- output$UniqueID
          }
          output$UniqueID <- NULL
          output
          
      }
      res1
  }

  if(parallel == TRUE){ stopCluster(cl) }

  # Change variable names
  names(Results)[names(Results) == 'trial'] <-  trial
  names(Results)[names(Results) == 'genotype'] <- genotype
  if(plot != "none"){
  names(Results)[names(Results) == 'plot'] <- plot
  }
  if(rep != "none"){
  names(Results)[names(Results) == 'rep'] <- rep
  }
  names(Results)[names(Results) == 'time'] <- timevar
  names(Results)[names(Results) == 'prTrait_S'] <- paste0(trait,'_S')
  names(Results)[names(Results) == 'prTrait_L'] <- paste0(trait,'_L')
  
  return(Results)
}  
 

#################### Get Mean squared error (MSE) for each curve ###################
mseCurve <- function(fittedData, rawData, timeVar, fittedPheno, rawPheno,
                     trial, plantID){
  names(fittedData)[names(fittedData)==fittedPheno]<-'fittedPheno'
  names(fittedData)[names(fittedData)==timeVar]<-'timeVar'
  names(fittedData)[names(fittedData)==trial]<-'trial'
  names(fittedData)[names(fittedData)==plantID]<-'plantID'
  names(rawData)[names(rawData)==rawPheno]<-'rawPheno'
  names(rawData)[names(rawData)==trial]<-'trial'
  names(rawData)[names(rawData)==plantID]<-'plantID'
  names(rawData)[names(rawData)==timeVar]<-'timeVar'
  d <- merge(rawData, fittedData[c('trial','plantID','timeVar','fittedPheno')], 
             by.x=c('trial','plantID','timeVar'),
             by.y=c('trial','plantID','timeVar'))
  d$trial.plantID <- paste(d$trial,d$plantID,sep='_')
  
  results <- foreach(i = unique(d$trial.plantID),.combine=rbind)%do%{
    s <- subset(d, trial.plantID==i)
    mse <- sum((s$rawPheno-s$fittedPheno)^2)/nrow(s)
    
    data.frame(trial=s$trial[1], plantID=s$plantID[1], 
               genotype = unique(s[grep(paste(c('gen','Gen'), collapse="|"), colnames(s))][1]), 
               MSE=mse)
  }
  return(results)
}



 
#################### Extract 'linear' part from curve ##################
extractCurve <- function(data,   # data output from growthCurves()
                         rep, plot, # 'none' if not existant
                         genotype, trial,
                         propMaxRate = 0.8, # the proportion of the max. growth rate below which the growth curve is considered 'non-linear'
                         curveType, # which type to base the cut-off on, logistic (L) or spline (S)?
                         parallel = TRUE)
{
  #Temporary change of var. names
  names(data)[names(data)==trial]    <- 'trial'
  names(data)[names(data)==genotype] <- 'genotype'
  if(plot != "none"){
    names(data)[names(data)==plot]     <- 'plot'
  } 
  if(rep != "none"){
    names(data)[names(data)==rep]      <- 'rep'
  }
  
  # Parallel or sequential?
  if(parallel == FALSE){
    registerDoSEQ()
    `%fun%` <- `%do%`
  } else {
    cl <- makePSOCKcluster(detectCores()-1)
    registerDoParallel(cl)
    `%fun%` <- `%dopar%`
  }
  
  ## Loop over experiments
  Results <- foreach(exp = unique(data$trial), .combine=rbind, .packages='doParallel') %fun% {
 
   # Combine names to create  unique identifier for each plot
   if(rep != "none" & plot != "none"){
     data$UniqueID <- paste(data$genotype,data$rep,data$plot, sep='_')
     uniqueID_constitute = 'GenoRepPlot'
   } else if(rep != "none" & plot == "none"){
     data$UniqueID <- paste(data$genotype,data$rep, sep='_')
     uniqueID_constitute = 'GenoRep'
   } else if(rep == "none" & plot != "none"){
     data$UniqueID <- paste(data$genotype,data$plot, sep='_') 
     uniqueID_constitute = 'GenoPlot'
   } else {
     data$UniqueID <- data$genotype
     uniqueID_constitute = 'Geno'
   }
   
   s <- subset(data, trial == exp)
   res1 <- foreach(i = unique(s$genotype), .combine=rbind) %do%{
     s1 <- subset(s, genotype == i)
     if(curveType == 'L'){
      s1$linear <- ifelse(s1$GrowthRate_L >= propMaxRate*unique(s1$MaxRate_L), 1, 0)
     } else {
      s1$linear <- ifelse(s1$GrowthRate_S >= propMaxRate*unique(s1$MaxRate_S), 1, 0)
     }
     s2 <- subset(s1, linear==1); s2$linear <- NULL
     s2
   } ; res1
  }
  if(parallel == TRUE){ stopCluster(cl) }
  
  # Change back variable names
  names(Results)[names(Results) == 'trial'] <-  trial
  names(Results)[names(Results) == 'genotype'] <- genotype
  if(plot != "none"){
    names(Results)[names(Results) == 'plot'] <- plot
  }
  if(rep != "none"){
    names(Results)[names(Results) == 'rep'] <- rep
  }
  Results$UniqueID <- NULL
  return(Results)
}



#################### Plot growth curves ##################

## CAUTION: has not been updated for datasets without rep or plotID

plotGrowthCurve <- function(data,        # dataset containing predicted heights from growthCurve() function
                            trial,       # column denoting trial (Exp_ID)
                            which.trial, # vector of trial names to be included in plot
                            predictedTrait, # height column predicted by spline/logistic curve (y axis)
                            time,        # time variable (x axis)
                            genotype,    # column containing genotypes
                            which.geno,  # vector of genotypes to be plotted
                            rep,         # replicate column; 'none' if not available
                            plot,        # plot column (useful if genotypes have multiple plots per replicate); 'none' if NA
                            which.rep = c(1,2))    # vector of replicates to be plotted
{
  # Plot dimensions
  l <- length(which.trial)
  if(l == 1){
    nrow = 1; ncol = 1
  } else {
   ncol = 2; nrow =  round_any(l, 2, f=ceiling)/2
  } 
  
  names(data)[names(data)==trial] <- 'trial'
  names(data)[names(data)==predictedTrait] <- 'predictedTrait'
  if(rep != 'none'){
    names(data)[names(data)==rep] <- 'rep'
  }
  names(data)[names(data)==genotype] <- 'genotype'
  if(plot != 'none'){
    names(data)[names(data)==plot] <- 'plot'
  }
  names(data)[names(data)==time] <- 'time'
  
  
  par(mfrow = c(nrow, ncol), mai=c(0.8, 0.9, 0.1, 0.06), oma=c(2,2.6,0,0))  

  for(exp in unique(data$trial)){
    s<-subset(data, trial==exp & rep %in% which.rep & genotype %in% which.geno)
    # In case Plot is not uniquely identifiable, make a variable with unique plot identification
    s$GenRepPlot <- as.factor(paste(s$genotype,s$rep,s$plot, sep='_')) # unique geno-rep-plot combination
    s <- transform(s, RecID = as.numeric(GenRepPlot)) # numeric of above
    
    plot(1, ylim=c(0,max(s$predictedTrait)),xlim=c(min(s$time),max(s$time)),ylab='height', xlab=time) 
    for(reps in unique(s$GenRepPlot)){
      points(s$height[s$GenRepPlot==reps] ~ s$time[s$GenRepPlot==reps], 
             col = rgb(70/255,130/255,180/255,0.2), type='l')
    }
    mtext(exp, side=3, line=-1.5, adj=0.05)
  }

}

 
