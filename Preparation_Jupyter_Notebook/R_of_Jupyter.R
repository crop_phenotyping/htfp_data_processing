################################################################################
#This is the final R code for exercise in juptyter notebook.
#autor: Olivia Zumsteg
#Date: 21.04.2023
################################################################################





setwd("C:/Users/zumstego/Documents/PhD/Vorlesungen/htfp_data_processing/Jupyter_trait_extraction")

inputPath <- "Data/"
outputPath <- "Output/Graphs/"


packages = c("nlme", "mgcv", "readr", "tidyr", "purrr", "ggplot2", "lubridate", "gridExtra", "broom", "zoo", "plyr", "stringr", "SpATS", "dplyr", "stats")
shh <- suppressMessages(suppressWarnings(lapply(c(packages), library, character.only = TRUE, warn.conflicts = FALSE)))
rm(shh)


suppressMessages(suppressWarnings(source("Data/models/Spline_QMER.R", verbose = FALSE, echo = FALSE)))
suppressMessages(suppressWarnings(source("Data/models/Dose_response.R", verbose = FALSE)))
suppressMessages(suppressWarnings(source("Data/models/FitSpATS.R", verbose = FALSE)))
suppressMessages(suppressWarnings(source("Data/models/Graphs.R", verbose = FALSE)))
suppressMessages(suppressWarnings(source("Data/models/PsplinesREML.R", verbose = FALSE)))



#read in data and structure columns
indat <- read.table(paste0(inputPath,"FPWW018_CnpHgt_CPCourse_subset.csv"),header = T, sep=",", skip=2) %>% 
  mutate_at(vars(2:7),as.factor) %>%  
  mutate(date = as.POSIXct(strptime(date, "%Y.%m.%d")), 
         height.cor = height.cor *1000) 

#rename columns: 
names(indat) = names(indat)  %>% 
  gsub("Plot_ID", "plot.UID", .) %>% 
  gsub("height.cor", "value", .) %>% 
  gsub("Gen_ID", "genotype_id", .) %>% 
  gsub("Gen_Name", "genotype_name", .)

#convert timestamp to date formate and remove unnecessary column
indat$timestamp = paste0(indat$date, " 10:13:00")
indat$timestamp = as.POSIXct(indat$timestamp, tz = "GMT", "%Y-%m-%d %H:%M:%S")
indat = indat %>% select(-Plot)



#read in design data 
design1 <- read.table(paste0(inputPath,"designs.csv"),header = T, sep=",")
design2 <- read.table(paste0(inputPath,"designs_2.csv"),header = T, sep=",")
design = bind_rows(design1, design2) %>% select(-genotype_id)

#merge design to data file 
data = left_join(indat, design, by=c("plot.UID" = "UID"))

#Read growing degree days as covariates
covFPWW018   <- read.csv(paste0(inputPath,"cov_FPWW018.csv"),header = T) %>% 
  # bring date in same format as other data
  mutate(date = as.POSIXct(as.character(Date), format = "%Y-%m-%d")) %>%  
  # calculate GDD for each date by cumulating the daily temperature
  mutate(GDD = cumsum(Tsum))

#add covariate information to the data
dat <- left_join(data,select(covFPWW018,date,GDD,Tmean), by="date")

# Add timepoint of preliminar measurement and value delta (growth) to each timepoint
df_values_for_fit <- dat %>%
  dplyr::group_by(plot.UID) %>% 
  dplyr::arrange(timestamp) %>% 
  mutate(lag_timestamp = dplyr::lag(timestamp), value_delta = value - lag(value))

# Remove first measurements, they do not suite as delta measurement
df_values_for_fit <- df_values_for_fit %>% filter(!is.na(lag_timestamp))

# Calculate measurement time point shifts in hours
df_values_for_fit <- df_values_for_fit %>%
  group_by(plot.UID) %>%
  mutate(time_diff = difftime(timestamp, lag_timestamp, units = "hours"))




#axis = "doy" or "GDD"
axis = "doy"

#fit a logistic regression
logistic_regression(df = df_values_for_fit, axis = "doy", p = "FPWW0180682", threshold_start = 1 / 4, threshold_stop = 1 / 4, final_height_agg = 24)


#remove an outlier: 
dat_rm = dat %>% filter(date != "2017-06-12")


# Add timepoint of preliminar measurement and value delta (growth) to each timepoint
df_values_for_fit_rm <- dat_rm %>%
  dplyr::group_by(plot.UID) %>% 
  dplyr::arrange(timestamp) %>% 
  mutate(lag_timestamp = dplyr::lag(timestamp), value_delta = value - lag(value))

# Remove first measurements, they do not suite as delta measurement
df_values_for_fit_rm <- df_values_for_fit_rm %>% filter(!is.na(lag_timestamp))

# Calculate measurement time point shifts in hours
df_values_for_fit_rm <- df_values_for_fit_rm %>%
  group_by(plot.UID) %>%
  mutate(time_diff = difftime(timestamp, lag_timestamp, units = "hours"))



#axis = "doy" or "GDD"
logistic_regression(df = df_values_for_fit_rm, axis = "GDD", p = "FPWW0180682", threshold_start = 1 / 5, threshold_stop = 1 / 4 , final_height_agg = 120)





splines(dat = df_values_for_fit, p = "FPWW0180682", threshold_start = 1 / 4, threshold_stop = 1 / 4, final_height_agg = 24)
splines(dat = df_values_for_fit_rm, p = "FPWW0180630", threshold_start = 1 / 4, threshold_stop = 1 / 4, final_height_agg = 24)


























