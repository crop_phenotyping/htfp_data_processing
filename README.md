&copy; Lukas Roth, Group of crop science, ETH Zurich

# Table of Contents
1. [**Phenomics data processing I**: A plot-level model for repeated measurements to extract the timing of key stages and quantities at defined time points](#htfp-data-processing-i)
2. [**Phenomics data processing II**: Extracting dose-response curve parameters from
high-resolution temperature courses and repeated field-based wheat height
measurements](#htfp-data-processing-ii)

# HTFP data processing I
<a name="HTFP1"></a>
Based on manuscript:

Roth, L., Rodríguez-Álvarez, M. X., van Eeuwijk, F., Piepho, H.-P. & Hund, A. Phenomics data processing: A plot-level model for repeated measurements to extract the timing of key stages and quantities at defined time points. F. Crop. Res. 274, (2021).
(https://doi.org/10.1016/j.fcr.2021.108314) 

## Demo

For a running minimal example see:
[1_Pspline_QMER_SpATS_asreml.R](Demo/1_Pspline_QMER_SpATS_asreml.R)

![](Graphs/1_Splines.png)
![](Graphs/1_QMER.png)
![](Graphs/1_BLUEs.png)

## Stage 1

P-spline and QMER method functions:
```R/Model/Spline_QMER.R```

## Stage 2
SpATS functions:
```R/Model/FitSpATS.R```

## Stage 3
asreml-R functions:
```R/Model/FitREML.R```

# HTFP data processing II
<a name="HTFP2"></a>
Based on manuscript:

Roth L, Piepho H-P, Hund A. 2022. Phenomics data processing: extracting dose–response curve parameters from high-resolution temperature courses
and repeated field-based wheat height measurements. In Silico Plants 2022: diac007; doi: (http://doi.org/10.1093/insilicoplants/diac007)

Botany.one blog post:

https://botany.one/2022/06/modelling-phenomics-data-for-temperature-response/

## Demo

For a running minimal example see:
[2_Temp_dose_response.R](Demo/2_Temp_dose_response.R)

![](Graphs/2_wang_engel_asym.png)

## Dose-Respone curve extraction function
```R/Model/Dose_response.R``` 
