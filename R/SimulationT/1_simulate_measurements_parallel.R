# Working directory with temperature data
path_home <- 'C:/Users/luroth/PycharmProjects/htfp_wheat_canopy_height_processing'
path_home <- './'
#path_simulation <- 'E:/Simulation/RunsT'
path_simulation <- './Simulation/RunsT'
setwd(path_home)

# Libraries to use
library(readr)
library(tidyr)
library(purrr)
library(ggplot2)
library(lubridate)
library(forecast)
library(gridExtra)
library(broom)
library(zoo)
library(plyr)
library(stringr)

library(dplyr)

library(foreach)
library(multidplyr)


source("./R/Model/Graphs.R")
source("./R/Model/Dose_response.R")

## Add temperature courses to corresponding years
extract_covar <- function(df, from, to) {
  df_ <- df %>% filter(timestamp > from, timestamp <= to)
  return(df_)
}

number_of_cpu <- 8
cluster <- new_cluster(number_of_cpu)
cluster_library(cluster, c("dplyr", "lubridate"))
cluster_copy(cluster, c("extract_covar"))
cluster_copy(cluster, c("loss_function", "optimize_", "defaults_"))
cluster_copy(cluster, c("growth_response_linear", "growth_response_linear_l", "lm_fit"))
cluster_copy(cluster, c("growth_response_wang_engel", "growth_response_wang_engel_l", "engel_fit"))

set.seed(1)

no_of_genotypes <- 1000


# Measurement error
# 1 cm Measurement error according to Agisoft Metashape
sigma_error <- 10

# Means and SDs according to field data 2016-2018:
mean_r <- 1.05
sd_r <- 0.12
# Other parameters according to Porter (mean) and field data 2016-2018 (sd)
# mean: 5.5
# range: -5 ... 10.65
mean_Tmin_ant <- 9.5
mean_Tmin_term <- 1.5
sd_Tmin <- 2
# mean: 15.8
# range: 10.65 ... 24.15
mean_Topt_ant <- 21
mean_Topt_term <- 10.6
sd_Topt <- 2
# mean: 32.5
# range: 24.15 ... 50
mean_Tmax_ant <- 35
mean_Tmax_term <- 30
sd_Tmax <- 2

mean_start <- c(108, 103, 101, 95, 100)
sd_start <- c(2.8, 3.0, 3.1, 3.0, 2.9)
mean_stop <- c(165, 162, 158, 164, 160)
sd_stop <- c(2.5, 3.5, 4.0, 3.0, 3.1)

cluster_copy(cluster, c("sd_stop", "mean_stop", "sd_start", "mean_start", "sd_Tmax", "mean_Tmax_term", "mean_Tmax_ant",
                        "sd_Tmin", "mean_Tmin_term", "mean_Tmin_ant", "sd_r", "mean_r", "sigma_error"))

# Genotypes
df_genotypes <- data.frame(genotype.id = seq(1, no_of_genotypes)) %>% arrange(genotype.id)
df_genotypes <- df_genotypes %>% mutate(genotype.name = paste0("Dummy Genotype ", seq(n())))

# Load covariates
df_temp <- read_csv('Simulation/covariate_temp.csv') %>% select(timestamp, value)
cluster_copy(cluster, c("df_temp"))

# Temperature courses
df_temp_sel <- df_temp %>%
  filter(yday(timestamp) > 60 & yday(timestamp) < 250) %>%
  mutate(year = year(timestamp)) %>%
  filter(year %in% c(2016, 2017, 2018, 2019, 2020))


# Prepare year_sites
year_sites <- tibble(year_site.harvest_year = c(2016, 2017, 2018, 2019, 2020)) %>%
  group_by(year_site.harvest_year) %>%
  partition(cluster) %>%
  mutate(temp_course = list(extract_covar(df_temp, ymd(paste0(as.numeric(as.character(year_site.harvest_year)) - 1, "1001")),
                                          ymd(paste0(as.character(year_site.harvest_year), "0801"))))) %>%
  collect()

# Prepare measurements
# One measurement frequency for simulation: 3 d (can be reduced later to any other frequency)
measurement_dates_freq_1d <- c(
  format(seq.Date(from = as.Date("2020-03-15"), as.Date("2020-07-15"), by = 1), "%m%d")
)

df_measurements <- expand_grid(year_sites, measurement_dates = measurement_dates_freq_1d)
df_measurements <- df_measurements %>% mutate(timestamp = parse_date_time(paste0(year_site.harvest_year, measurement_dates, "1200"), orders = "YmdHM"))
df_measurements$measurement_dates <- NULL

# Add timepoint of preliminar measurement
df_measurements <- df_measurements %>%
  group_by(year_site.harvest_year) %>%
  partition(cluster) %>%
  arrange(timestamp) %>%
  mutate(lag_timestamp = lag(timestamp)) %>%
  collect()
df_measurements <- df_measurements %>% filter(!is.na(lag_timestamp))

# Filter temp_course for time period
df_measurements$measurement.id <- 1:nrow(df_measurements)
df_measurements <- df_measurements %>%
  group_by(measurement.id) %>%
  partition(cluster) %>%
  mutate(temp_course = list(extract_covar(data.frame(temp_course), lag_timestamp, timestamp))) %>%
  collect()

## Generated start and end of stem elongation
df_genotypes_parameters <- df_genotypes %>% mutate(start_growth = round(
  0 + rnorm(n(), mean = 0, sd = mean(sd_start))))
df_genotypes_parameters <- df_genotypes_parameters %>% mutate(stop_growth = round(
  0 + rnorm(n(), mean = 0, sd = mean(sd_stop))))
# Generate growth response parameters
df_genotypes_parameters <- df_genotypes_parameters %>% mutate(Tmin = abs(rnorm(n(), mean = mean(c(mean_Tmin_ant, mean_Tmin_term)), sd = sd_Tmin)))
df_genotypes_parameters <- df_genotypes_parameters %>% mutate(r = abs(rnorm(n(), mean = mean_r, sd = sd_r)))
df_genotypes_parameters <- df_genotypes_parameters %>% mutate(Topt = abs(rnorm(n(), mean = mean(c(mean_Topt_ant, mean_Topt_term)), sd = sd_Topt)))
df_genotypes_parameters <- df_genotypes_parameters %>% mutate(Tmax = abs(rnorm(n(), mean = mean(c(mean_Tmax_ant, mean_Tmax_term)), sd = sd_Tmax)))
df_genotypes_parameters <- df_genotypes_parameters %>% filter(Tmax > Topt, Topt > Tmin)

df_genotypes_parameters_with_mean <- df_genotypes_parameters %>%
  mutate(start_growth = start_growth + mean(mean_start),
         stop_growth = stop_growth + mean(mean_stop))
write_csv(df_genotypes_parameters_with_mean, paste0(path_simulation, "/genotypes_params.csv"))

# Add year-site intercept to start and stop of growth
df_genotypes_parameters <- expand_grid(df_genotypes_parameters, year_sites %>% select(year_site.harvest_year)) %>%
  rowwise() %>%
  partition(cluster) %>%
  mutate(
    start_growth = case_when(
      year_site.harvest_year == 2016 ~ round(start_growth * (sd_start[1] / mean(sd_start)) + mean_start[1]),
      year_site.harvest_year == 2017 ~ round(start_growth * (sd_start[2] / mean(sd_start)) + mean_start[2]),
      year_site.harvest_year == 2018 ~ round(start_growth * (sd_start[3] / mean(sd_start)) + mean_start[3]),
      year_site.harvest_year == 2019 ~ round(start_growth * (sd_start[4] / mean(sd_start)) + mean_start[4]),
      year_site.harvest_year == 2020 ~ round(start_growth * (sd_start[5] / mean(sd_start)) + mean_start[5]),
      TRUE ~ start_growth),
    stop_growth = case_when(
      year_site.harvest_year == 2016 ~ round(stop_growth * (sd_stop[1] / mean(sd_stop)) + mean_stop[1]),
      year_site.harvest_year == 2017 ~ round(stop_growth * (sd_stop[2] / mean(sd_stop)) + mean_stop[2]),
      year_site.harvest_year == 2018 ~ round(stop_growth * (sd_stop[3] / mean(sd_stop)) + mean_stop[3]),
      year_site.harvest_year == 2019 ~ round(stop_growth * (sd_stop[4] / mean(sd_stop)) + mean_stop[4]),
      year_site.harvest_year == 2020 ~ round(stop_growth * (sd_stop[5] / mean(sd_stop)) + mean_stop[5]),
      TRUE ~ stop_growth)
  ) %>%
  collect()

df_plot_values <- inner_join(df_genotypes_parameters, df_measurements)

# Cardinal temp ramps
df_plot_values <- df_plot_values %>% mutate(
  Tmin_ramp = Tmin - (mean_Tmin_ant - mean_Tmin_term) / 2 + (yday(timestamp) - start_growth) /
    (stop_growth - start_growth) * (mean_Tmin_ant - mean_Tmin_term),
  Topt_ramp = Topt - (mean_Topt_ant - mean_Topt_term) / 2 + (yday(timestamp) - start_growth) /
    (stop_growth - start_growth) * (mean_Topt_ant - mean_Topt_term),
  Tmax_ramp = Tmax - (mean_Tmax_ant - mean_Tmax_term) / 2 + (yday(timestamp) - start_growth) /
    (stop_growth - start_growth) * (mean_Tmax_ant - mean_Tmax_term)
)


# GROW LITTLE PLANTS, GROW!
df_plot_values <- df_plot_values %>%
  group_by(measurement.id, genotype.id) %>%
  partition(cluster) %>%
  mutate(fixed = growth_response_wang_engel_l(data.frame(temp_course)$value %>% list(),
                                              NULL,
                                              Tmin,
                                              Topt,
                                              Tmax,
                                              r),
         ramp = growth_response_wang_engel_l(data.frame(temp_course)$value %>% list(),
                                             NULL,
                                             Tmin_ramp,
                                             Topt_ramp,
                                             Tmax_ramp,
                                             r)) %>%
  collect()

df_plot_values <- df_plot_values %>% pivot_longer(cols = c(ramp, fixed), names_to = "cardinal_model",
                                                  values_to = "height_delta")

# Reset growth out of period to zero
df_plot_values <- df_plot_values %>%
  rowwise() %>%
  partition(cluster) %>%
  mutate(height_delta = if_else(start_growth > yday(timestamp), 0, height_delta)) %>%
  collect()
df_plot_values <- df_plot_values %>%
  rowwise() %>%
  partition(cluster) %>%
  mutate(height_delta = if_else(stop_growth < yday(timestamp), 0, height_delta)) %>%
  collect()

df_plot_values <- df_plot_values %>%
  group_by(genotype.id, year_site.harvest_year, cardinal_model) %>%
  partition(cluster) %>%
  arrange(timestamp) %>%
  mutate(value = cumsum(height_delta)) %>%
  collect()

df_genotype_yearsite_params <- df_genotypes_parameters %>% select(
  year_site.harvest_year,
  genotype.id,
  Tmin, Topt, Tmax, r)

# Save for validation step
write_csv(df_genotype_yearsite_params, paste0(path_simulation, "/genotype_yearsite_params.csv"))

# Add measurement noise

# noise to height delta

# Influence of other covariates
df_E_other_covars <- df_plot_values %>%
  ungroup() %>%
  select(year_site.harvest_year, genotype.id, measurement.id) %>%
  unique() %>%
  group_by(year_site.harvest_year, genotype.id) %>%
  partition(cluster) %>%
  mutate(E_other_covars = arima.sim(list(order = c(1, 0, 0), ar = 0.9), n = n(), sd = 1)) %>%
  collect()

# Measurement
df_E_measurement <- df_plot_values %>%
  ungroup() %>%
  select(year_site.harvest_year, genotype.id, measurement.id) %>%
  unique() %>%
  mutate(E_measurement = arima.sim(list(order = c(1, 0, 0), ar = 0.3), n = n(), sd = sigma_error))

# systematic measurement error
df_E_measurement_sys <- df_plot_values %>%
  ungroup() %>%
  select(year_site.harvest_year, measurement.id) %>%
  unique() %>%
  mutate(E_measurement_systematic = arima.sim(list(order = c(1, 0, 0), ar = 0.3), n = n(), sd = sigma_error))

# Add all errors
df_plot_values <- inner_join(inner_join(inner_join(df_plot_values, df_E_other_covars), df_E_measurement), df_E_measurement_sys)
df_plot_values <- df_plot_values %>%
  ungroup() %>%
  mutate(
    RAW = value,
    RAW_with_SysEm = value + E_measurement_systematic,
    RAW_with_Em = value + E_measurement,
    RAW_with_Em_and_Ecov = value + E_measurement + E_other_covars) %>%
  select(-value) %>%
  pivot_longer(cols = c(RAW, RAW_with_SysEm, RAW_with_Em, RAW_with_Em_and_Ecov), names_to = "error_model", values_to = "value")


### PLOT
# Growth response curves
df_growth_response <- data.frame(
  temp = seq(0, 30, 0.01))
df_growth_response <- expand_grid(df_growth_response, df_genotypes_parameters_with_mean)
df_growth_response <- df_growth_response %>%
  mutate(sim_engel =
           growth_response_wang_engel(df_growth_response$temp, Tmin, Topt, Tmax, r))
df_growth_response$sim <- "Genotypic"


plot_models <- ggplot(data = df_growth_response %>% group_by(genotype.id) %>% nest() %>% ungroup() %>% sample_n(200) %>% unnest()) +
  geom_line(aes(x = temp, y = sim_engel, group = genotype.id), alpha = 0.4) +
  scale_y_continuous(expression(paste("Simulated growth rate (", mm ~ h^-1, ")"))) +
  scale_x_continuous(expression(paste("Temperature (", degree * C, ")"))) +
  theme +
  facet_wrap(~sim) +
  theme(legend.position = "bottom")
plot_models


plot_growth <- ggplot(data = df_plot_values %>%
  filter(cardinal_model == "ramp", error_model == "RAW_with_Em_and_Ecov",
  ) %>%
  filter(yday(timestamp) %% 3 == 0) %>% filter(year_site.harvest_year %in% c(2016, 2018, 2020)) %>%
  group_by(genotype.id) %>% nest() %>% ungroup() %>% sample_n(200) %>% unnest(),
                      aes(x = timestamp, y = value / 1000, group = genotype.id)) +
  geom_line(alpha = 0.2) +
  facet_grid(~year_site.harvest_year, scales = "free_x") +
  scale_x_datetime(NULL) +
  scale_y_continuous("Simulated canopy height (m)", breaks = c(0, 0.5, 1, 1.5)) +
  theme
plot_growth

plot_all <- grid.arrange(plot_models, plot_growth, widths = c(1, 2))

ggsave(filename = paste0("Graphs/SimulationT/Overview_plots.pdf"), plot_all, width = 14, height = 6)


## Save simulated measurements
df_plot_values <- df_plot_values %>% mutate(method.id = 1, method.name = "Simulated height measurements",
                                            trait.id = 1, trait.name = "Canopy height", trait.label = "CnpHeight",
                                            trait.ontology_id = "CO_321:0000020",
                                            responsible = "luroth")
write_csv(df_plot_values %>% select(genotype.id, error_model, cardinal_model, year_site.harvest_year, method.id, method.name, trait.id, trait.name, trait.label, responsible, timestamp, value, start_growth, stop_growth),
          paste0(path_simulation, "/trait_values.csv"))

