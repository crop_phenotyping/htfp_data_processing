# Working directory with temperature data
path_home <- 'C:/Users/luroth/PycharmProjects/htfp_data_processing'
path_simulation <- 'E:/Simulation/Runs'
setwd(path_home)

# Libraries to use
library(readr)
library(tidyr)
library(purrr)
library(ggplot2)
library(lubridate)
library(forecast)
library(gridExtra)
library(broom)
library(zoo)
library(plyr)
library(stringr)

library(dplyr)

library(foreach)


source("R/Model/Graphs.R")
source("R/Model/Dose_response.R")


number_of_cpus <- 35
set.seed(1)

max_runs <- 500

# Measurement error
# 1 cm Measurement error according to Agisoft Metashape
sigma_error <- 10

# Means and SDs according to field data 2016-2018:
mean_bplm_Asym <- 1.05
sd_bplm_Asym <- 0.12
# Kemp1982: Minimum around 8 deg
mean_bplm_c0 <- 8.0
sd_bplm_c0 <- 2
# Kemp1982: Optimum around 25 deg
mean_bplm_cOpt <- 18
sd_bplm_cOpt <- 2

mean_start <- c(108, 103, 101)
sd_start <- c(2.8, 3.0, 3.1)
mean_stop <- c(165, 162, 158)
sd_stop <- c(2.5, 3.5, 4.0)

coef_ar1 <- 0.95
r_var_field2pop_response <- 0.01
r_var_field2pop_timing <- 1

# Load designs and genotype parameters
df_designs <- read_csv('Simulation/designs.csv')
df_genotypes <- read_csv('Simulation/genotypes.csv')

# Load covariates
df_temp <- read_csv('Simulation/covariate_temp.csv') %>% select(timestamp, value)

# Temperature courses
df_temp_sel <- df_temp %>%
  filter(yday(timestamp) > 60 & yday(timestamp) < 250) %>%
  mutate(year = year(timestamp)) %>%
  filter(year %in% c(2016, 2017, 2018))

## Add temperature courses to corresponding years
extract_covar <- function(df, from, to) {
  df_ <- df %>% filter(timestamp > from, timestamp <= to)
  return(df_)
}

# Prepare year_sites
year_sites <- tibble(year_site.harvest_year = df_designs$year_site.harvest_year %>% unique()) %>%
  group_by(year_site.harvest_year) %>%
  mutate(temp_course = list(extract_covar(df_temp, ymd(paste0(as.numeric(as.character(year_site.harvest_year)) - 1, "1001")),
                                          ymd(paste0(as.character(year_site.harvest_year), "0801")))))

# Prepare measurements
# One measurement frequency for simulation: 1 d (can be reduced later to any other frequency)
measurement_dates_freq_1d <- c(
  format(seq.Date(from = as.Date("2020-03-01"), as.Date("2020-07-20"), by = 1), "%m%d")
)

df_measurements <- expand_grid(year_sites, measurement_dates = measurement_dates_freq_1d)
df_measurements <- df_measurements %>% mutate(timestamp = parse_date_time(paste0(year_site.harvest_year, measurement_dates, "1200"), orders = "YmdHM"))
df_measurements$measurement_dates <- NULL

# Add timepoint of preliminar measurement
df_measurements <- df_measurements %>%
  group_by(year_site.harvest_year) %>%
  arrange(timestamp) %>%
  mutate(lag_timestamp = lag(timestamp))
df_measurements <- df_measurements %>% filter(!is.na(lag_timestamp))

# Filter temp_course for time period
df_measurements$measurement.id <- 1:nrow(df_measurements)
df_measurements <- df_measurements %>%
  group_by(measurement.id) %>%
  mutate(temp_course = list(extract_covar(data.frame(temp_course), lag_timestamp, timestamp)))

print(paste0("Initiationg clusters (", number_of_cpus, ") and start simulating data"))

cl <- parallel::makeCluster(number_of_cpus)
doParallel::registerDoParallel(cl)
foreach(run = 1:max_runs,
        .packages = c("splines", "lubridate", "purrr", "tibble", "plyr", "dplyr", "readr", "tidyr", "ggplot2", "gridExtra", "broom", "zoo", "stringr")
) %dopar% {

#run <- 1
#for (run in 1:max_runs) {

  print(paste("Generating set", run))

  if (!file.exists(paste0(path_simulation, "/", run, "/trait_values.csv"))) {

    dir.create(paste0(path_simulation, "/", run), showWarnings = FALSE)

    ## Generated start and end of stem elongation
    df_genotypes_parameters <- df_genotypes %>% mutate(start_growth = round(
      0 + rnorm(n(), mean = 0, sd = mean(sd_start))))
    df_genotypes_parameters <- df_genotypes_parameters %>% mutate(stop_growth = round(
      0 + rnorm(n(), mean = 0, sd = mean(sd_stop))))
    # Generate growth response parameters
    df_genotypes_parameters <- df_genotypes_parameters %>% mutate(bplm_c0 = abs(rnorm(n(), mean = mean_bplm_c0, sd = sd_bplm_c0)))
    df_genotypes_parameters <- df_genotypes_parameters %>% mutate(bplm_Asym = abs(rnorm(n(), mean = mean_bplm_Asym, sd = sd_bplm_Asym)))
    df_genotypes_parameters <- df_genotypes_parameters %>% mutate(bplm_cOpt = abs(rnorm(n(), mean = mean_bplm_cOpt, sd = sd_bplm_cOpt)))
    df_genotypes_parameters <- df_genotypes_parameters %>% mutate(bplm_cOpt = if_else(bplm_cOpt < bplm_c0, bplm_c0+1, bplm_cOpt))
    
    df_genotypes_parameters_with_mean <- df_genotypes_parameters %>%
      mutate(start_growth = start_growth + mean(mean_start),
             stop_growth = stop_growth + mean(mean_stop))
    write_csv(df_genotypes_parameters_with_mean, paste0(path_simulation, "/", run, "/genotypes_params.csv"))
    
    # Add year-site intercept to start and stop of growth
    df_genotypes_parameters <- inner_join(df_genotypes_parameters, df_designs) %>%
      rowwise() %>%
      mutate(
        start_growth = case_when(
          year_site.harvest_year == 2016 ~ round(start_growth * (sd_start[1] / mean(sd_start)) + mean_start[1]),
          year_site.harvest_year == 2017 ~ round(start_growth * (sd_start[2] / mean(sd_start)) + mean_start[2]),
          year_site.harvest_year == 2018 ~ round(start_growth * (sd_start[3] / mean(sd_start)) + mean_start[3]),
          TRUE ~ start_growth),
        stop_growth = case_when(
          year_site.harvest_year == 2016 ~ round(stop_growth * (sd_stop[1] / mean(sd_stop)) + mean_stop[1]),
          year_site.harvest_year == 2017 ~ round(stop_growth * (sd_stop[2] / mean(sd_stop)) + mean_stop[2]),
          year_site.harvest_year == 2018 ~ round(stop_growth * (sd_stop[3] / mean(sd_stop)) + mean_stop[3]),
          TRUE ~ stop_growth)
      )
    
    # ADD PLOT NOISE (caused by other covariates)
    # Intercept
    df_E_c0_row <- df_genotypes_parameters %>%
      ungroup() %>%
      select(plot.row, year_site.UID) %>%
      unique() %>%
      mutate(E_c0_row = arima.sim(list(order = c(1, 0, 0), ar = coef_ar1), n = n(), sd = (sd_bplm_c0*r_var_field2pop_response)/sqrt(2)))
    df_E_c0_range <- df_genotypes_parameters %>%
      ungroup() %>%
      select(plot.range, year_site.UID) %>%
      unique() %>%
      mutate(E_c0_range = arima.sim(list(order = c(1, 0, 0), ar = coef_ar1), n = n(), sd = (sd_bplm_c0*r_var_field2pop_response)/sqrt(2)))
    # cOpt
    df_E_cOpt_row <- df_genotypes_parameters %>%
      ungroup() %>%
      select(plot.row, year_site.UID) %>%
      unique() %>%
      mutate(E_cOpt_row = arima.sim(list(order = c(1, 0, 0), ar = coef_ar1), n = n(), sd = (sd_bplm_cOpt*r_var_field2pop_response)/sqrt(2)))
    df_E_cOpt_range <- df_genotypes_parameters %>%
      ungroup() %>%
      select(plot.range, year_site.UID) %>%
      unique() %>%
      mutate(E_cOpt_range = arima.sim(list(order = c(1, 0, 0), ar = coef_ar1), n = n(), sd = (sd_bplm_cOpt*r_var_field2pop_response)/sqrt(2)))
    # Asym
    df_E_Asym_row <- df_genotypes_parameters %>%
      ungroup() %>%
      select(plot.row, year_site.UID) %>%
      unique() %>%
      mutate(E_Asym_row = arima.sim(list(order = c(1, 0, 0), ar = coef_ar1), n = n(), sd = (sd_bplm_Asym*r_var_field2pop_response)/sqrt(2)))
    df_E_Asym_range <- df_genotypes_parameters %>%
      ungroup() %>%
      select(plot.range, year_site.UID) %>%
      unique() %>%
      mutate(E_Asym_range = arima.sim(list(order = c(1, 0, 0), ar = coef_ar1), n = n(), sd = (sd_bplm_Asym*r_var_field2pop_response)/sqrt(2)))
    # Start SE
    df_E_start_row <- df_genotypes_parameters %>%
      ungroup() %>%
      select(plot.row, year_site.UID) %>%
      unique() %>%
      mutate(E_start_row = arima.sim(list(order = c(1, 0, 0), ar = coef_ar1), n = n(), sd = (mean(sd_start)*r_var_field2pop_timing)/sqrt(2)))
    df_E_start_range <- df_genotypes_parameters %>%
      ungroup() %>%
      select(plot.range, year_site.UID) %>%
      unique() %>%
      mutate(E_start_range = arima.sim(list(order = c(1, 0, 0), ar = coef_ar1), n = n(), sd = (mean(sd_start)*r_var_field2pop_timing)/sqrt(2)))
    # Stop SE
    df_E_stop_row <- df_genotypes_parameters %>%
      ungroup() %>%
      select(plot.row, year_site.UID) %>%
      unique() %>%
      mutate(E_stop_row = arima.sim(list(order = c(1, 0, 0), ar = coef_ar1), n = n(), sd = (mean(sd_stop)*r_var_field2pop_timing)/sqrt(2)))
    df_E_stop_range <- df_genotypes_parameters %>%
      ungroup() %>%
      select(plot.range, year_site.UID) %>%
      unique() %>%
      mutate(E_stop_range = arima.sim(list(order = c(1, 0, 0), ar = coef_ar1), n = n(), sd = (mean(sd_stop)*r_var_field2pop_timing)/sqrt(2)))
    
    
    df_genotypes_parameters <- inner_join(inner_join(inner_join(inner_join(inner_join(inner_join(inner_join(inner_join(inner_join(inner_join(
      df_genotypes_parameters, 
      df_E_c0_row), df_E_c0_range), 
      df_E_cOpt_row), df_E_cOpt_range), 
      df_E_Asym_row), df_E_Asym_range),
      df_E_start_range), df_E_start_row),
      df_E_stop_range), df_E_stop_row) %>%
      mutate(bplm_c0 = bplm_c0 + E_c0_range + E_c0_row,
             bplm_cOpt = bplm_cOpt + E_cOpt_range + E_cOpt_row,
             bplm_Asym = bplm_Asym + E_Asym_range + E_Asym_row,
             start_growth = start_growth + E_start_range + E_start_row,
             stop_growth = stop_growth + E_stop_range + E_stop_row
      )
    df_genotypes_parameters <- df_genotypes_parameters %>% mutate(bplm_cOpt = if_else(bplm_cOpt < bplm_c0, bplm_c0, bplm_cOpt))
    

    df_plot_values <- inner_join(df_genotypes_parameters, df_measurements)
    
    # GROW LITTLE PLANTS, GROW!
    df_plot_values <- df_plot_values %>%
      group_by(measurement.id, plot.UID) %>%
      mutate(height_delta = growth_response_bp_linear_l(data.frame(temp_course)$value %>% list(),
                                                        NULL,
                                                        bplm_c0,
                                                        bplm_cOpt,
                                                        bplm_Asym))
    # Reset growth out of period to zero
    df_plot_values <- df_plot_values %>%
      rowwise() %>%
      mutate(height_delta = if_else(start_growth > yday(timestamp), 0, height_delta))
    df_plot_values <- df_plot_values %>%
      rowwise() %>%
      mutate(height_delta = if_else(stop_growth < yday(timestamp), 0, height_delta))
    
    df_plot_values <- df_plot_values %>%
      group_by(plot.UID) %>%
      arrange(timestamp) %>%
      mutate(value = cumsum(height_delta))
    
    # extract final height
    df_final_height <- df_plot_values %>% group_by(year_site.UID,
                                plot.UID,
                                genotype.id) %>% 
      summarize(final_height = max(value))
    
    df_genotype_yearsite_params <- inner_join(df_genotypes_parameters %>% select(
      year_site.UID,
      plot.UID,
      genotype.id,
      start_growth, stop_growth, bplm_c0, bplm_cOpt, bplm_Asym
    ), df_final_height)
    
    # Save for validation step
    write_csv(df_genotype_yearsite_params, paste0(path_simulation, "/", run, "/genotype_yearsite_params.csv"))
    
    # Add measurement noise
    
    # noise to height delta

    # Measurement
    df_E_measurement <- df_plot_values %>%
      ungroup() %>%
      select(year_site.UID, measurement.id) %>%
      unique() %>%
      mutate(E_measurement = arima.sim(list(order = c(1, 0, 0), ar = 0.7), n = n(), sd = sigma_error))
    # Plot rows
    df_E_row <- df_plot_values %>%
      ungroup() %>%
      select(plot.row, year_site.UID) %>%
      unique() %>%
      mutate(E_row = arima.sim(list(order = c(1, 0, 0), ar = 0.7), n = n(), sd = sigma_error / 50))
    # Plot range
    df_E_range <- df_plot_values %>%
      ungroup() %>%
      select(plot.range, year_site.UID) %>%
      unique() %>%
      mutate(E_range = arima.sim(list(order = c(1, 0, 0), ar = 0.7), n = n(), sd = sigma_error / 50))
    # Add all errors
    df_plot_values <- inner_join(inner_join(inner_join(df_plot_values, df_E_range), df_E_row), df_E_measurement)
    df_plot_values <- df_plot_values %>%
      ungroup() %>%
      mutate(value = value +
        E_measurement +
        E_row +
        E_range +
        rnorm(n(), mean = 0, sd = sigma_error / 100))

    if (run == 1) {
      # Growth response curves
      df_growth_response <- data.frame(
        temp = seq(0, 30, 0.01))
      df_growth_response <- expand_grid(df_growth_response, df_genotypes_parameters_with_mean)
      df_growth_response <- df_growth_response %>%
        mutate(sim_lm =
                 growth_response_bp_linear(df_growth_response$temp, bplm_c0, bplm_cOpt, bplm_Asym))
      df_growth_response$sim <- "Genotypic"
      
      
      plot_models <- ggplot(data = df_growth_response) +
        geom_line(aes(x = temp, y = sim_lm, group = genotype.id), alpha = 0.4) +
        scale_y_continuous(expression(paste("Simulated growth rate (", mm ~ h^-1, ")"))) +
        scale_x_continuous(expression(paste("Temperature (", degree * C, ")"))) +
        theme +
        facet_wrap(~sim) + 
        theme(legend.position = "bottom")
      plot_models

      # plot_temp <- ggplot(data = df_temp_sel, aes(x = value)) +
      #   stat_density(geom = "line") +
      #   facet_wrap(~year, nrow = 2) +
      #   scale_x_continuous(expression(paste("Temperature (", degree * C, ")"))) +
      #   scale_y_continuous("Density") +
      #   theme
      # plot_temp

      plot_growth <- ggplot(data = df_plot_values, aes(x = timestamp, y = value / 1000, group = plot.UID)) +
        geom_line(alpha=0.2) +
        facet_wrap(~year_site.harvest_year, scales = "free_x") +
        scale_x_datetime(NULL) +
        scale_y_continuous("Simulated canopy height (m)") +
        theme
      plot_growth

      plot_all <- grid.arrange(plot_models, plot_growth, widths = c(1, 2))

      ggsave(filename = paste0("Graphs/Simulation/Overview_plots.pdf"), plot_all, width = 14, height = 6)
    }

    ## Save simulated measurements
    df_plot_values <- df_plot_values %>% mutate(method.id = 1, method.name = "Simulated height measurements",
                                                trait.id = 1, trait.name = "Canopy height", trait.label = "CnpHeight",
                                                trait.ontology_id = "CO_321:0000020",
                                                responsible = "luroth")
    write_csv(df_plot_values %>% select(plot.UID, method.id, method.name, trait.id, trait.name, trait.label, responsible, timestamp, value),
              paste0(path_simulation, "/", run, "/trait_values.csv"))
  } else {
    print("Exists")
  }
}
